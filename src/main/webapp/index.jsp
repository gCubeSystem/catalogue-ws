<html>
<body>
	<h2>gCube Catalogue WS is up and running!</h2>
	<div>
	<p>You can find all information about the D4Science data Catalogue <a href="https://wiki.gcube-system.org/gcube/GCube_Data_Catalogue">here</a>.</p>
	<p>If you are a developer, you are suggested to visit our <a href="https://dev.d4science.org/">developer site</a>.</p>
	<br>
	<br>
	<p>gCube Team</p>
	</div>
</body>
</html>
